# 2020 协力抗疫 - 码力全开 线上公益黑客马拉松

[**活动流程及报名信息请点我**](flow.md)

## 介绍
2020 年的春天，一场始料不及的新型冠状病毒肺炎席卷而来。

疫情还在继续，在家隔离，就是最好的支持。

然而，除了呆在家，我们还可以做些什么？是否可以在全民抗疫的特殊时期，贡献自己的一份力量？

我们不难看到 ：

居委会为了做好疫情监控，他们需要挨家挨户登记每户健康情况；企业 HR ，他们一次又一次群发通知，收集、汇总、整理、更新公司每个人的健康情况；媒体不断发布寻找同行人的消息，希望找到潜在的病毒携带者，切断传染；各医院、药店、机构寻求口罩等医护资源；

**疫情之下，隔离不等于”停止“一切** ：

老师学生需要上课，企业需要正常办公，物流运输不能停···

疫情期，大家都在极力维持正常运转。

作为互联网人，在全民抗疫情之下，可以做些什么？疫情之下，有哪些被忽略的潜在需求？我们是否可以通过一些小工具 ，帮助疫情监控、帮助缓解疫情传播，帮助疫情中需要远程办公的中小企业，帮助身边需要帮助的人？
“协力抗疫，码力全开”，腾讯云云开发，协同 SegmentFault、码云Gitee、微信极客、云加社区，一起发起一场线上公益黑客马拉松，诚邀全国广大互联网人参与，一起为抗击疫情贡献自己的一份力量！

## 活动详情

活动说明：根据疫情期间，企业、医院、学校机构及个人等社会各场景应用需求，征集有助于全社会度过疫情期间的工作生活等方方面面的小程序应用创意，最后代码开源供全社会免费使用。

### 一、日程安排

![](https://postimg.aliavv.com/mbp/7zd9v.jpg)

### 二、参与规则

1. 报名人数：不限；
2. 团队规模：自由组队，最多不超过4人；
3. 作品载体：小程序，且运行在小程序·云开发上；
4. 全新代码：为公平起见，所有人都是同时开始编码。任何人都不允许在原有项目基础上编码；更不允许直接使用开源代码；
5. 代码审查：路演前，需要将代码提交到码云Gitee接受代码审查，从而确保代码是全新的且不存在作弊、抄袭现象；
6. 知识产权：参赛团队将具有团产品的全部知识产权和所有权，对产品可以自由支配，建议活动作品开源，同时默认授权给所有主办方进行二次传播；
7. 产品展示：每组需按要求提供图片、代码等素材，在云加社区进行作品展示，以作为网上投票参考；
8. 作品评分：由评委分（60%）和 人气分（40%）两部分组成；
9. 最终活动解释权归主办方所有。

### 三、活动评委嘉宾

![](https://postimg.aliavv.com/mbp/vp0qa.jpg)


### 四、评选与礼品

- 一等奖 2名 哈曼卡顿音响 + 证书
- 二等奖 3名 Apple AirPods + 证书
- 三等奖 5名 Kindle 电子书阅读器 + 证书

*实体礼品以团队为单位，证书以人为单位每人一份*

**扫码进活动咨询群，了解更多详情**

![](https://postimg.aliavv.com/mbp/ylpqv.jpg)

[**活动流程及报名信息请点我**](flow.md)
